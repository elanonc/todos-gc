module.exports = {
	env: {
		es2021: true,
		node: true,
	},
	extends: [
		'eslint:recommended',
		'plugin:@typescript-eslint/recommended',
		'plugin:prettier/recommended',
	],
	parser: '@typescript-eslint/parser',
	parserOptions: {
		ecmaVersion: 'latest',
		sourceType: 'module',
	},
	plugins: ['@typescript-eslint'],
	ignorePatterns: ['node_modules', '.turbo', 'coverage', 'dist'],
	rules: {
		'no-console': 'error',
		'no-magic-numbers': 'error',
		'no-nested-ternary': 'error',
		'prefer-const': 'error',
		'prefer-template': 'warn',
		'arrow-spacing': 'warn',
	},
};
