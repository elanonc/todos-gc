import { Router } from 'express';

import CollectionController from './CollectionController';
import authenticate from 'middlewares/auth';
import { validate } from 'middlewares/validate';
import collectionSchema from './schema/collection';

const collectionController = new CollectionController();
const collectionRoutes = Router();

collectionRoutes.use(authenticate);

collectionRoutes.get('/', collectionController.index);
collectionRoutes.post(
	'/',
	validate(collectionSchema),
	collectionController.create,
);

export default collectionRoutes;
