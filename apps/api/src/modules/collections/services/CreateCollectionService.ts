import { Collection } from '@prisma/client';
import {
	CreateCollectionDTO,
	ICollectionRepository,
} from '../repository/ICollectionRepository';

class CreateCollectionService {
	constructor(private collectionRepository: ICollectionRepository) {}

	async execute(collection: CreateCollectionDTO): Promise<Collection> {
		return this.collectionRepository.create(collection);
	}
}

export default CreateCollectionService;
