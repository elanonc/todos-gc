import { Request, Response } from 'express';
import { StatusCode } from 'status-code-enum';
import CollectionRepository from './repository/CollectionRepository';
import CreateCollectionService from './services/CreateCollectionService';
import ListCollectionsService from './services/ListCollectionsService';

const collectionRepository = new CollectionRepository();

class CollectionController {
	async index(req: Request, res: Response) {
		const { id } = res.locals.user;
		const listCollectionsService = new ListCollectionsService(
			collectionRepository,
		);

		const collections = await listCollectionsService.execute(id);

		res.send(collections);
	}

	async create(req: Request, res: Response) {
		const { name, color } = req.body;
		const { id } = res.locals.user;
		const createCollectionService = new CreateCollectionService(
			collectionRepository,
		);

		const collection = await createCollectionService.execute({
			name,
			color,
			userId: id,
		});

		res.status(StatusCode.SuccessCreated).send(collection);
	}
}

export default CollectionController;
