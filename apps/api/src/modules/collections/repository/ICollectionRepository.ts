import { Collection } from '@prisma/client';
import { CollectionWithInclude } from './CollectionRepository';

export type CreateCollectionDTO = Omit<Collection, 'id' | 'createdAt'>;

export interface ICollectionRepository {
	create(collection: CreateCollectionDTO): Promise<Collection>;
	findByUserId(userId: string): Promise<CollectionWithInclude[]>;
	findByName(collection: string): Promise<Collection>;
}
