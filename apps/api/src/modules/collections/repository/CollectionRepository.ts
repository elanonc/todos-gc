import { Collection, Todo } from '@prisma/client';
import { prismaClient } from 'database/prismaClient';
import {
	CreateCollectionDTO,
	ICollectionRepository,
} from './ICollectionRepository';

export type CollectionWithInclude = { todos: Todo[] } & Collection;

class CollectionRepository implements ICollectionRepository {
	findByName(collection: string): Promise<Collection> {
		return prismaClient.collection.findFirst({
			where: {
				name: collection,
			},
		});
	}

	create(collection: CreateCollectionDTO): Promise<Collection> {
		return prismaClient.collection.create({
			data: collection,
			include: {
				todos: true,
			},
		});
	}

	findByUserId(userId: string): Promise<CollectionWithInclude[]> {
		return prismaClient.collection.findMany({
			where: { userId },
			include: {
				todos: true,
			},
		});
	}
}

export default CollectionRepository;
