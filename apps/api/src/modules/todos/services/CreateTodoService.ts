import { StatusCode } from 'status-code-enum';
import AppError from 'error/AppError';
import { ICollectionRepository } from 'modules/collections/repository/ICollectionRepository';
import { ICreateTodoDTO, ITodoRepository } from '../repository/ITodoRepository';

class CreateTodoService {
	constructor(
		private todoRepository: ITodoRepository,
		private collectionRepository: ICollectionRepository,
	) {}

	async execute(todo: ICreateTodoDTO) {
		const collection = await this.collectionRepository.findByName(
			todo.collection,
		);

		if (todo.collection && !collection) {
			throw new AppError(
				'This collection does not exist',
				StatusCode.ClientErrorBadRequest,
			);
		}

		return this.todoRepository.create(todo);
	}
}

export default CreateTodoService;
