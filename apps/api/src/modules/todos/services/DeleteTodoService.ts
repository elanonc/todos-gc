import { StatusCode } from 'status-code-enum';
import AppError from 'error/AppError';
import { ITodoRepository } from '../repository/ITodoRepository';

class DeleteTodoService {
	constructor(private todoRepository: ITodoRepository) {}

	async execute(id: string) {
		const verifyTodo = await this.todoRepository.findById(id);

		if (!verifyTodo) {
			throw new AppError('Todo not found', StatusCode.ClientErrorNotFound);
		}

		this.todoRepository.deleteById(id);
	}
}

export default DeleteTodoService;
