import { Request, Response } from 'express';
import { StatusCode } from 'status-code-enum';
import CreateTodoService from './services/CreateTodoService';
import DeleteTodoService from './services/DeleteTodoService';
import UpdateTodoService from './services/UpdateTodoService';
import TodoRepository from './repository/TodoRepository';
import ListUserTodosService from './services/ListUserTodosService';
import CollectionRepository from 'modules/collections/repository/CollectionRepository';

const collectionRepository = new CollectionRepository();
const todoRepository = new TodoRepository();

class TodoController {
	async index(req: Request, res: Response) {
		const { id } = res.locals.user;

		const listTodosService = new ListUserTodosService(todoRepository);

		const todos = await listTodosService.execute(id);

		res.send(todos);
	}

	async create(req: Request, res: Response) {
		const { title, description, collection } = req.body;
		const { id } = res.locals.user;

		const createTodoService = new CreateTodoService(
			todoRepository,
			collectionRepository,
		);
		const todo = await createTodoService.execute({
			title,
			description,
			collection,
			userId: id,
		});

		res.status(StatusCode.SuccessCreated).send(todo);
	}

	async update(req: Request, res: Response) {
		const { title, description, completed } = req.body;
		const { id } = req.params;

		const updateTodoService = new UpdateTodoService(todoRepository);
		const todo = await updateTodoService.execute(id, {
			title,
			description,
			completed,
		});

		res.send(todo);
	}

	async delete(req: Request, res: Response) {
		const { id } = req.params;

		const deleteTodoService = new DeleteTodoService(todoRepository);
		await deleteTodoService.execute(id);

		res
			.status(StatusCode.SuccessOK)
			.send({ message: 'Todo deleted successfully' });
	}
}

export default TodoController;
