import Joi from 'joi';

const TITLE_MAX_LENGTH = 45;
const DESCRIPTION_MAX_LENGTH = 150;

const todoSchema = Joi.object({
	title: Joi.string().max(TITLE_MAX_LENGTH).required().messages({
		'string.max': 'title should be less than 45 characters',
		'string.empty': 'Title and description are required',
	}),
	description: Joi.string()
		.max(DESCRIPTION_MAX_LENGTH)
		.message('description should be less than 150 characters')
		.required()
		.messages({
			'string.max': 'description should be less than 150 characters',
			'string.empty': 'Title and description are required',
		}),
	completed: Joi.boolean().optional(),
	collection: Joi.string().optional(),
});

export { todoSchema };
