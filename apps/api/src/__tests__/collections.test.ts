import { User } from '@prisma/client';
import { StatusCode } from 'status-code-enum';
import { randomUUID } from 'crypto';
import { prismaClient } from 'database/prismaClient';
import { sign } from 'jsonwebtoken';
import request from 'supertest';
import { app } from '../index';

const req = request(app);
const endpoint = `${process.env.API_PREFIX}/collections`;

describe('/collections', () => {
	const session: Partial<{
		token: string;
		user: User;
	}> = {};

	beforeAll(async () => {
		const userId = randomUUID();
		session.token = sign({}, String(process.env.APP_SECRET), {
			subject: userId,
		});
		session.user = await prismaClient.user.create({
			data: {
				email: 'test@test.com',
				password: 'test',
				name: 'Test',
				id: userId,
			},
		});

		await prismaClient.collection.createMany({
			data: [
				{
					name: 'School',
					createdAt: new Date().toISOString(),
					id: randomUUID(),
					color: '#ff0000',
					userId,
				},
				{
					name: 'Work',
					createdAt: new Date().toISOString(),
					id: randomUUID(),
					color: '#321ff1',
					userId,
				},
			],
		});
	});

	describe('GET /collections', () => {
		it('should list collections', async () => {
			const collections = await req
				.get(endpoint)
				.set('Authorization', `Bearer ${session.token}`);

			expect(collections.status).toBe(StatusCode.SuccessOK);
			expect(collections.body).toStrictEqual([
				{
					id: expect.any(String),
					createdAt: expect.any(String),
					name: 'School',
					color: expect.any(String),
					userId: session.user.id,
					_count: {
						todos: 0,
						completedTodos: 0,
					},
				},
				{
					id: expect.any(String),
					createdAt: expect.any(String),
					name: 'Work',
					color: expect.any(String),
					userId: session.user.id,
					_count: {
						todos: 0,
						completedTodos: 0,
					},
				},
			]);
		});
	});

	describe('POST /collections', () => {
		it('should create a collection', async () => {
			const collection = await req
				.post(endpoint)
				.set('Authorization', `Bearer ${session.token}`)
				.send({
					name: 'Test',
					color: '#ff00ff',
				});

			expect(collection.status).toBe(StatusCode.SuccessCreated);
			expect(collection.body).toStrictEqual({
				id: collection.body.id,
				createdAt: collection.body.createdAt,
				name: collection.body.name,
				color: collection.body.color,
				todos: [],
				userId: session.user.id,
			});
		});

		it('should validate name', async () => {
			const collection = await req
				.post(endpoint)
				.set('Authorization', `Bearer ${session.token}`)
				.send({
					name: 'Very Large Name That Should Fail',
					color: '#ff00ff',
				});

			expect(collection.status).toBe(StatusCode.ClientErrorUnprocessableEntity);
			expect(collection.body).toStrictEqual({
				message: 'Name must be less than 15 characters',
				status: StatusCode.ClientErrorUnprocessableEntity,
			});
		});

		it('should throw error if name is not passed', async () => {
			const collection = await req
				.post(endpoint)
				.set('Authorization', `Bearer ${session.token}`)
				.send({
					color: '#ff00ff',
				});

			expect(collection.status).toBe(StatusCode.ClientErrorUnprocessableEntity);
			expect(collection.body).toStrictEqual({
				message: 'Name is required',
				status: StatusCode.ClientErrorUnprocessableEntity,
			});
		});

		it('should validate color', async () => {
			const collection = await req
				.post(endpoint)
				.set('Authorization', `Bearer ${session.token}`)
				.send({
					name: 'Test',
					color: '#ff122faa0',
				});

			expect(collection.status).toBe(StatusCode.ClientErrorUnprocessableEntity);
			expect(collection.body).toStrictEqual({
				message: 'Color must be a valid hex color',
				status: StatusCode.ClientErrorUnprocessableEntity,
			});
		});

		it('should throw error if color is not passed', async () => {
			const collection = await req
				.post(endpoint)
				.set('Authorization', `Bearer ${session.token}`)
				.send({
					name: 'Test',
				});

			expect(collection.status).toBe(StatusCode.ClientErrorUnprocessableEntity);
			expect(collection.body).toStrictEqual({
				message: 'Color is required',
				status: StatusCode.ClientErrorUnprocessableEntity,
			});
		});
	});
});
