import request from 'supertest';
import { StatusCode } from 'status-code-enum';
import { app } from '../index';

const req = request(app);
const endpoint = `${process.env.API_PREFIX}/session`;

describe('/session', () => {
	describe('POST /session', () => {
		it('should create a session', async () => {
			await req.post(`${process.env.API_PREFIX}/user`).send({
				email: 'sessiontest@test.com',
				password: '12345678',
				name: 'Test',
			});

			const session = await req
				.post(endpoint)
				.send({ email: 'sessiontest@test.com', password: '12345678' });
			expect(session.status).toBe(StatusCode.SuccessOK);
			expect(session.body).toStrictEqual({
				token: expect.any(String),
				user: {
					id: expect.any(String),
					name: 'Test',
					email: 'sessiontest@test.com',
					createdAt: expect.any(String),
				},
			});
		});

		it('should not create a session with invalid email', async () => {
			const session = await req
				.post(endpoint)
				.send({ email: 'invald-email@invalid.com', password: 'test' });

			expect(session.status).toBe(StatusCode.ClientErrorUnauthorized);
			expect(session.body).toStrictEqual({
				message: 'Invalid credentials',
				status: StatusCode.ClientErrorUnauthorized,
			});
		});

		it('should not create a session with invalid password', async () => {
			await req.post(endpoint).send({
				email: 'test@test.com',
				password: '123',
				name: 'Test',
			});

			const session = await req
				.post(endpoint)
				.send({ email: 'test@test.com', password: 'invalid-password' });

			expect(session.status).toBe(StatusCode.ClientErrorUnauthorized);
			expect(session.body).toStrictEqual({
				message: 'Invalid credentials',
				status: StatusCode.ClientErrorUnauthorized,
			});
		});
	});
});
