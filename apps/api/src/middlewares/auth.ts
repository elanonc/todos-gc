import { verify } from 'jsonwebtoken';
import { StatusCode } from 'status-code-enum';
import AppError from 'error/AppError';
import { NextFunction, Request, Response } from 'express';

async function authenticate(
	request: Request,
	response: Response,
	next: NextFunction,
) {
	const authHeader = request.headers.authorization;

	if (!authHeader) {
		throw new AppError(
			'JWT token is missing',
			StatusCode.ClientErrorUnauthorized,
		);
	}

	const [, token] = authHeader.split(' ');

	try {
		const { sub: userId } = verify(token, String(process.env.APP_SECRET));

		response.locals.user = {
			id: userId,
		};

		next();
	} catch (err) {
		throw new AppError('Invalid JWT token', StatusCode.ClientErrorUnauthorized);
	}
}

export default authenticate;
