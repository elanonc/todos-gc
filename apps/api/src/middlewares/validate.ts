import { NextFunction, Request, Response } from 'express';
import { StatusCode } from 'status-code-enum';
import { Schema } from 'joi';

const validate = (schema: Schema) => {
	return (req: Request, res: Response, next: NextFunction) => {
		const { error } = schema.validate(req.body);

		if (error) {
			return res.status(StatusCode.ClientErrorUnprocessableEntity).send({
				status: 422,
				message: error.details.map(err => err.message).join(', '),
			});
		}

		next();
	};
};

export { validate };
