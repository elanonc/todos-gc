import { useSession } from 'next-auth/react';
import { FiArrowRight } from 'react-icons/fi';
import { Checkbox } from 'components/common/Checkbox';
import { MyLink } from 'components/common/MyLink';
import { Disclosure } from 'components/Disclosure';
import { Header } from 'components/Header';

const getGreeting = () => {
	const date = new Date();
	const hours = date.getHours();

	const greetings = {
		'Good morning': hours >= 6 && hours < 12,
		'Good afternoon': hours >= 12 && hours < 18,
		'Good evening': hours >= 18 && hours < 24,
		'Good night': hours >= 0 && hours < 6,
	};

	return Object.keys(greetings).find(
		greeting => greetings[greeting as keyof typeof greetings],
	);
};

const Dashboard = () => {
	const { data: session } = useSession();
	return (
		<>
			<Header />
			<main className="mx-auto max-w-xl">
				<h1 className="my-12 text-xl font-medium">Dashboard</h1>

				<p className="text-4xl font-bold leading-snug">
					{getGreeting()}, <br /> {session?.user.name}
				</p>

				<div className="mt-8">
					<Disclosure
						title="Design"
						footerContent={
							<nav className="rounded-b-2xl border-t-2 border-main-700 bg-main-850 p-4">
								<MyLink
									className="flex items-center justify-center"
									href={`/collection/design`}
								>
									<p>Go to collection </p>
									<FiArrowRight />
								</MyLink>
							</nav>
						}
					>
						<div className="w-max">
							<Checkbox id="checkbox" label="Label text" />
						</div>
					</Disclosure>
				</div>
			</main>
		</>
	);
};

export default Dashboard;
