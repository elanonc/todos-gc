import type { AppProps } from 'next/app';
import { SessionProvider } from 'next-auth/react';
import { QueryClientProvider } from '@tanstack/react-query';
import { queryClient } from 'lib/react-query-client';
import 'styles/globals.scss';

const MyApp = ({
	Component,
	pageProps: { session, ...pageProps },
}: AppProps) => (
	<SessionProvider session={session}>
		<QueryClientProvider client={queryClient}>
			<Component {...pageProps} />
		</QueryClientProvider>
	</SessionProvider>
);

export default MyApp;
