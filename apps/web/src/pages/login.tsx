import Link from 'next/link';
import { SubmitHandler, useForm } from 'react-hook-form';
import { FiMail, FiLock, FiAlertCircle } from 'react-icons/fi';
import { z } from 'zod';
import { Bubble } from 'components/Bubble';
import { Button } from 'components/common/Button';
import { Input } from 'components/common/Input';
import { zodResolver } from '@hookform/resolvers/zod';
import { signIn } from 'next-auth/react';
import { useState } from 'react';
import { useRouter } from 'next/router';

const MIN_PASSWORD_LENGTH = 8;

const loginSchema = z.object({
	email: z.string().email(),
	password: z.string().min(MIN_PASSWORD_LENGTH, {
		message: `Password must be at least ${MIN_PASSWORD_LENGTH} characters`,
	}),
});

type LoginProps = z.infer<typeof loginSchema>;

const Login = () => {
	const [invalidCredentials, setInvalidCredentials] = useState(false);
	const router = useRouter();
	const {
		register,
		handleSubmit,
		formState: { errors },
	} = useForm<LoginProps>({
		resolver: zodResolver(loginSchema),
	});

	const onSubmit: SubmitHandler<LoginProps> = async ({ email, password }) => {
		const status = await signIn('credentials', {
			callbackUrl: '/dashboard',
			redirect: false,
			email,
			password,
		});

		if (status?.ok && status.url) {
			router.push(status.url);
			setInvalidCredentials(false);
		} else {
			setInvalidCredentials(true);
		}
	};

	return (
		<div className="px-8">
			<section className="mx-auto flex h-screen w-full flex-col justify-center md:w-96">
				<h1 className="mb-2 text-4xl font-light">Log in</h1>
				<p className="text-sm font-bold uppercase text-gray-600">Welcome</p>
				<form
					className="my-8 flex flex-col gap-4"
					onSubmit={handleSubmit(onSubmit)}
				>
					<Input
						icon={FiMail}
						type="email"
						placeholder="youremail@email.com"
						errorMessage={errors.email?.message}
						{...register('email')}
					/>
					<Input
						icon={FiLock}
						type="password"
						placeholder="Your password"
						errorMessage={errors.password?.message}
						{...register('password')}
					/>
					{invalidCredentials && (
						<span className="flex items-center text-rose-500">
							<FiAlertCircle className="w-4 h-4 mr-2" />
							Invalid credentials
						</span>
					)}
					<p className="mb-4 text-gray-500">
						Doesn&apos;t have an account ?
						<Link href="/sign-in">
							<a className="ml-2 text-pink-400 underline">Sign in</a>
						</Link>
					</p>
					<Button text="Log in" type="submit" />
				</form>
			</section>

			<Bubble variant="BLACK" size={160} right={300} top={140} />
			<Bubble size={140} bottom={200} right="25%" />
			<Bubble size={200} top={250} left="10%" />
			<Bubble variant="BLACK" size={200} bottom="10%" left="25%" />
		</div>
	);
};

export default Login;
