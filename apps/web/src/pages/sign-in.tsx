import Link from 'next/link';
import { useRouter } from 'next/router';
import { useForm } from 'react-hook-form';
import { FiUser, FiMail, FiLock } from 'react-icons/fi';
import { zodResolver } from '@hookform/resolvers/zod';
import { useMutation } from '@tanstack/react-query';
import { z } from 'zod';

import { createUser } from 'api';
import { Bubble } from 'components/Bubble';
import { Button } from 'components/common/Button';
import { Input } from 'components/common/Input';

const MIN_PASSWORD_LENGTH = 8;
const MIN_NAME_LENGTH = 3;

const createUserSchema = z
	.object({
		name: z.string().min(MIN_NAME_LENGTH, {
			message: `Name must be at least ${MIN_NAME_LENGTH} characters`,
		}),
		email: z.string().email(),
		password: z.string().min(MIN_PASSWORD_LENGTH, {
			message: `Password must be at least ${MIN_PASSWORD_LENGTH} characters`,
		}),
		confirm_password: z.string(),
	})
	.refine(data => data.password === data.confirm_password, {
		message: 'Passwords do not match',
		path: ['confirm_password'],
	});

export type CreateUserProps = z.infer<typeof createUserSchema>;

const SignIn = () => {
	const router = useRouter();
	const {
		register,
		handleSubmit,
		formState: { errors },
	} = useForm<CreateUserProps>({
		resolver: zodResolver(createUserSchema),
	});

	const mutation = useMutation(createUser, {
		onSuccess: () => router.push('/login'),
	});

	return (
		<div className="px-8">
			<section className="mx-auto flex h-screen w-full flex-col justify-center md:w-96">
				<h1 className="mb-2 text-4xl font-light">Sign in</h1>
				<p className="text-sm font-bold uppercase text-gray-600">Welcome</p>

				<form
					className="my-8 flex flex-col gap-4"
					// eslint-disable-next-line @typescript-eslint/no-unused-vars
					onSubmit={handleSubmit(({ confirm_password, ...rest }) => {
						mutation.mutate(rest);
					})}
				>
					<Input
						icon={FiUser}
						type="text"
						placeholder="Your name"
						errorMessage={errors.name?.message}
						{...register('name')}
					/>
					<Input
						icon={FiMail}
						type="email"
						placeholder="youremail@email.com"
						errorMessage={errors.email?.message}
						{...register('email')}
					/>
					<Input
						icon={FiLock}
						type="password"
						placeholder="Must have 8 characters"
						errorMessage={errors.password?.message}
						{...register('password')}
					/>
					<Input
						icon={FiLock}
						type="password"
						placeholder="Confirm your password"
						errorMessage={errors.confirm_password?.message}
						{...register('confirm_password')}
					/>
					<p className="mb-4 text-gray-500">
						Already have an account ?
						<Link href="/login">
							<a className="ml-2 text-pink-400 underline">Log in</a>
						</Link>
					</p>
					<Button text="Sign in" type="submit" />
				</form>
			</section>
			<Bubble variant="BLACK" size={160} right={300} top={140} />
			<Bubble size={140} bottom={200} right="25%" />
			<Bubble size={200} top={250} left="10%" />
			<Bubble variant="BLACK" size={200} bottom="10%" left="25%" />
		</div>
	);
};

export default SignIn;
