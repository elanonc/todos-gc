import { Disclosure as HeadlessDisclosure } from '@headlessui/react';
import { ReactNode } from 'react';
import { FiChevronUp, FiChevronDown } from 'react-icons/fi';

type CollectionDisclosureProps = {
	title: string;
	children: ReactNode;
	footerContent: ReactNode;
	defaultOpen?: boolean;
};

const Disclosure = ({
	footerContent,
	title,
	children,
	defaultOpen = true,
}: CollectionDisclosureProps) => (
	<HeadlessDisclosure defaultOpen={defaultOpen}>
		{({ open }) => (
			<div className="w-full max-w-xl">
				<HeadlessDisclosure.Button className="flex w-full items-center justify-between rounded-t-2xl bg-main-700 p-4">
					<p
						className="font-medium
					"
					>
						{title}
					</p>
					{open ? (
						<FiChevronUp className="text-gray-500" />
					) : (
						<FiChevronDown className="text-gray-500" />
					)}
				</HeadlessDisclosure.Button>
				<HeadlessDisclosure.Panel className="bg-main-850 px-4 py-6">
					{children}
				</HeadlessDisclosure.Panel>
				{footerContent}
			</div>
		)}
	</HeadlessDisclosure>
);

export { Disclosure };
