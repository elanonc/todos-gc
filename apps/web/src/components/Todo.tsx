import { Disclosure } from '@headlessui/react';
import { FiChevronUp, FiChevronDown } from 'react-icons/fi';
import { RiNodeTree } from 'react-icons/ri';
import { Checkbox } from './common/Checkbox';

type TodoProps = {
	label: string;
	id: string;
	subtodos?: TodoProps[];
};

const Todo = ({ id, label, subtodos }: TodoProps) => {
	if (subtodos?.length) {
		return (
			<Disclosure defaultOpen>
				{({ open }) => (
					<div className="w-full max-w-xl">
						<Disclosure.Button className="flex items-center justify-between bg-main-750 rounded-3xl py-4 px-3 w-full">
							<div>
								<Checkbox label={label} id={id} />
								<div className="flex items-center text-gray-400 ml-9 text-sm">
									<RiNodeTree className="mr-1" />
									<span>0/1</span>
								</div>
							</div>
							{open ? (
								<FiChevronUp className="text-gray-300 w-6 h-6" />
							) : (
								<FiChevronDown className="text-gray-300 w-6 h-6" />
							)}
						</Disclosure.Button>
						<Disclosure.Panel className="pl-4 mt-2">
							{subtodos.map(subtodo => (
								<div
									key={subtodo.id}
									className="bg-main-750 rounded-3xl py-4 px-3 w-full"
								>
									<Checkbox label={subtodo.label} id={subtodo.id} />
								</div>
							))}
						</Disclosure.Panel>
					</div>
				)}
			</Disclosure>
		);
	}

	return (
		<div className="bg-main-750 rounded-3xl py-4 px-3 w-full">
			<Checkbox label={label} id={id} />
		</div>
	);
};

export { Todo };
