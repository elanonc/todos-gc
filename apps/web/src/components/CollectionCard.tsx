import { MyLink } from './common/MyLink';

const CollectionCard = () => (
	<MyLink href="#" className=" bg-main-750 p-4 rounded-2xl hover:bg-main-700">
		<div>
			<div className="w-12 h-4 bg-pink-500 rounded-xl mb-4" />
			<p className="font-medium text-lg">School</p>
			<div>
				<p className="text-neutral-500 text-sm mt-2 font-medium">4/8 done</p>
			</div>
		</div>
	</MyLink>
);

export { CollectionCard };
