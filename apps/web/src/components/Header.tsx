import { signOut, useSession } from 'next-auth/react';
import { MdCollectionsBookmark, MdDashboard } from 'react-icons/md';
import { AddTodoPopover } from './AddTodoPopover';
import { Button } from './common/Button';
import { MyLink } from './common/MyLink';

const getInitials = (name: string) => {
	const names = name.split(' ');
	return names.length > 1
		? names.map(name => name[0]).join('')
		: names[0].slice(0, 2);
};

const Header = () => {
	const { data: session } = useSession();

	return (
		<header className="hidden h-14 w-full items-center justify-between border-b border-b-main-600 bg-main-800 px-6 py-4 sm:flex">
			<div className="flex items-center space-x-6">
				<MyLink
					href="/dashboard"
					className="flex items-center text-neutral-400 hover:text-neutral-300"
				>
					<MdDashboard className="mr-1 h-4 w-4" />
					<span className="text-sm">Dashboard</span>
				</MyLink>
				<MyLink
					href="/dashboard/collections"
					className="flex items-center text-neutral-400 hover:text-neutral-300"
				>
					<MdCollectionsBookmark className="mr-1 h-4 w-4" />
					<span className="text-sm">Collections</span>
				</MyLink>
			</div>

			<div className="flex items-center justify-between gap-4 w-52">
				<AddTodoPopover />
				<div className="flex items-center justify-center rounded-full bg-pink-50 p-2 text-sm text-pink-500 w-8 h-8">
					<p aria-label={session?.user.name}>
						{getInitials(session?.user.name ?? 'Not found')}
					</p>
				</div>
				<Button
					text="Sign out"
					onClick={() => signOut()}
					className="max-w-sm font-normal"
				/>
			</div>
		</header>
	);
};

export { Header };
