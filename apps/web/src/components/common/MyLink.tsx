import Link, { LinkProps } from 'next/link';
import { forwardRef, ReactNode } from 'react';

type MyLinkProps = LinkProps & {
	children: ReactNode;
	className?: string;
};

const MyLink = forwardRef<HTMLAnchorElement, MyLinkProps>(
	({ children, href, ...rest }, ref) => (
		<Link href={href}>
			<a ref={ref} {...rest}>
				{children}
			</a>
		</Link>
	),
);

MyLink.displayName = 'MyLink';

export { MyLink };
