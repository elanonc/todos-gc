import classNames from 'classnames';
import { ChangeEvent, useState } from 'react';

type CheckboxProps = {
	label: string;
	id: string;
	disabled?: boolean;
};

const Checkbox = ({ id, label, disabled = false }: CheckboxProps) => {
	const [isChecked, setIsChecked] = useState(false);
	const [isFocused, setIsFocused] = useState(false);

	const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
		setIsChecked(event.target.checked);
	};

	return (
		<label
			htmlFor={id}
			className={classNames(
				{
					'opacity-60 cursor-not-allowed': disabled,
				},
				'flex cursor-pointer items-center gap-2',
			)}
		>
			<input
				className="sr-only"
				type="checkbox"
				id={id}
				disabled={disabled}
				onChange={handleChange}
				onFocus={() => setIsFocused(true)}
				onBlur={() => setIsFocused(false)}
			/>

			<svg
				width="24"
				height="24"
				viewBox="0 0 20 21"
				fill="none"
				xmlns="http://www.w3.org/2000/svg"
				className={classNames(
					{
						'ring-1 ring-offset-1 ring-offset-current ring-secondary':
							isFocused,
						'text-gray-400': disabled,
						'text-secondary': !disabled,
					},
					'rounded-lg transition duration-300',
				)}
			>
				{/* background */}
				<rect
					x="1"
					y="1.5"
					width="18"
					height="18"
					rx="6"
					fill={isChecked ? 'currentColor' : 'none'}
				/>
				{/* checkmark */}
				<path
					d="M6 11.5L9 13.5L14.5 7.5"
					stroke={isChecked ? 'white' : 'transparent'}
					strokeWidth="2.01011"
					strokeLinecap="round"
					strokeLinejoin="round"
				/>
				{/* border */}
				<rect
					x="1"
					y="1.5"
					width="18"
					height="18"
					rx="6"
					stroke="currentColor"
					strokeWidth="2"
				/>
			</svg>
			<span
				className={classNames({
					'line-through': isChecked,
				})}
			>
				{label}
			</span>
		</label>
	);
};

export { Checkbox };
