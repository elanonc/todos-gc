<h1 align='center'>🚧 Under construction 🚧</h1>

<h1 align='center'>tasks</h1>
<p align='center'>A todo list app that I'm building to practice my skills building an entire app by myself</p>

<h1 align="center">MODIFICACOES PARA TRABALHO FINAL DE GC<h1>>
<p align="center"></p>

## 🛠 Technologies

This project was developed with the following technologies:

Frontend

- [Turborepo](https://turborepo.org/)
- [Jest](https://jestjs.io/pt-BR/)
- [Testing Library](https://testing-library.com/)
- [React Query](https://react-query.tanstack.com/)
- [Tailwind](https://tailwindcss.com/)
- [ReactJS](https://pt-br.reactjs.org)
- [Next.js](https://nextjs.org)
- [Typescript](typescriptlang.org/)

Backend

- [Turborepo](https://turborepo.org/)
- [Jest](https://jestjs.io/pt-BR/)
- [NodeJS](https://nodejs.org/)
- [Typescript](https://typescriptlang.org/)
- [Express](http://expressjs.com/pt-br/)
- [Prisma](https://www.prisma.io/)
- [Postgresql](https://www.postgresql.org/)
- [Docker](https://www.docker.com)

## 📱💻 Intruções 

```bash
## 1. Clone o repositório
git clone https://gitlab.com/elanonc/todos-gc.git

## 2. Abra a pasta do projeto
cd todos-gc

## 3. Instale as dependencias
yarn

## 4. Instale o postgresql e configure o seu banco de dados
## 5. Configure um arquivo .env com os dados do seu banco de dados com base no .env.example
cd apps/api 

## 5. Run migrations
cd apps/api && npx prisma migrate dev

## 6. Volte para a raíz do projeto e inicie o front e o back juntos 
yarn dev
```

## 🐳 Docker

Para rodar a aplicação com docker, após configurar o `.env` e antes de executar `yarn dev` você precisa executar 
`docker-compose up -d` para criar e iniciar um container executandno uma imagme postgres 

## 🤔 How to contribute

- Fork this repository;
- Create a branch with your feature: `git checkout -b my-feature`;
- Commit your changes: `git commit -m 'feat: My new feature'`;
- Push to your branch: `git push origin my-feature`.

Once your pull request has been merged, you can delete your branch.

## 📝 License

This project is under the MIT license. See the [LICENSE](https://github.com/guivictorr/todos/blob/master/LICENSE.md) file for more details.
